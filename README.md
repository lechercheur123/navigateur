# README #



### What is this repository for? ###

Ce script sert à afficher un message si le navigateur du client est trop ancien pour le site auquel il souhaite accéder.

### How do I get set up? ###

Pour le faire fonctionner, il faut d'abord installer la bibliothèque [WhichBrowser](https://github.com/NielsLeenheer/WhichBrowser). Ensuite, entrez ce code dans l'en tête entre des balises <script type="text/javascript"></script> :
 
```
#!javascript

(function WhichBrowser(){var p=[],w=window,d=document,e=f=0;p.push('ua='+encodeURIComponent(navigator.userAgent));e|=w.ActiveXObject?1:0;e|=w.opera?2:0;e|=w.chrome?4:0;
    e|='getBoxObjectFor' in d || 'mozInnerScreenX' in w?8:0;e|=('WebKitCSSMatrix' in w||'WebKitPoint' in w||'webkitStorageInfo' in w||'webkitURL' in w)?16:0;
    e|=(e&16&&({}.toString).toString().indexOf("\n")===-1)?32:0;p.push('e='+e);f|='sandbox' in d.createElement('iframe')?1:0;f|='WebSocket' in w?2:0;
    f|=w.Worker?4:0;f|=w.applicationCache?8:0;f|=w.history && history.pushState?16:0;f|=d.documentElement.webkitRequestFullScreen?32:0;f|='FileReader' in w?64:0;
    p.push('f='+f);p.push('r='+Math.random().toString(36).substring(7));p.push('w='+screen.width);p.push('h='+screen.height);var s=d.createElement('script');
    s.src='http://localhost:8888/nav/detect.js?' + p.join('&');d.getElementsByTagName('head')[0].appendChild(s);})();
    	// On crée la fonction "navigateur"
		  function navigateur()
		  {
			   Browsers = new WhichBrowser();
			   // On récupère le nom du navigateur
			   var browser = Browsers.browser.name;
			   // On le met en minuscule pour éviter les erreurs
			   browser = browser.toLowerCase();
			   // On récupère la vesion du navigateur, si il en a une. Sinon, on met 0
			   if (Browsers.browser.version != null) var version = Browsers.browser.version.major;
			   else var version = 0;
			   // On détermine si le navigateur est "vieux"
			   var old = ((browser != "chrome" && browser != "firefox" && browser != "safari") || (browser == "internet explorer" && version < 10))
			   if (old)
			   {
			   // On affiche le message si le navigateur est "vieux" (je n'arrive pas à faire autrement que d'effacer toute la page)
			   document.write("<p>Votre navigateur est trop ancien, merci de télécharger un navigateur comme Chrome, Firefox, ou Internet Explorer 10 ou supérieur. Pour télécharger Chrome, c'est <a href=\"www.google.com/intl/fr_fr/chrome/\">ici</a> et pour Firefox <a href=\"https://www.mozilla.org/fr/firefox/new/\">là</a>.</p>");
			   }
		}
```
##Attention ! Il faut modifier la ligne :

```
#!javascript

s.src='http://localhost:8888/nav/detect.js?'
```
par le chemin correspondant au fichier detect.js (en fait, c'est le fichier detect.php, mais mettez .js dans le chemin) de la bibliothèque téléchargée précédemment. Ne pas oublier le point d'interrogation.

##Attention ! Autoriser les fichier .htaccess pour cet emplacement (celui de la bibliothèque).